// sql.go : sql datatypes and methods
package main

import (
    "database/sql"
    "time"
)


// Database extends (by embedding) sql.DB
type Database struct {

    *sql.DB

    dsn string  // data source name (connection string)
    driver string // driver name
    stmts map[string]*sql.Stmt // statements

}


// Create a new Database, open and test it. Also refer to the dicumentation for
// OpenAndPing.  If this doesn't return an error, clients should
// `defer database.Close()`
func NewDatabase(driver, dsn string) (*Database, error) {

    d := Database{ driver: driver, dsn: dsn }

    err := d.OpenAndPing()
    if err != nil {
        return nil, err
    }

    d.stmts = make(map[string]*sql.Stmt)

    return &d, nil
}


// Open the database and test it. If the test fails, the connection is closed.
// If this doesn't return an error, clients should `defer database.db.Close()`
func (d *Database) OpenAndPing() error {

    var err error

    d.DB, err = sql.Open(d.driver, d.dsn)
    if err != nil {
        return err
    }

    err = d.Ping()
    if err != nil {
        d.Close()
        return err
    }


    return nil

}


// Add a named prepared statement to the databases
func (d *Database) AddStatement(name, query string) error {

    prepped, err := d.Prepare(query)
    if err != nil {
        return err
    }

    d.stmts[name] = prepped
    return nil

}


// Get a named prepared statement from the database
// Will panic if the statement is not in the store
func (d *Database) GetStatement(name string) *sql.Stmt {
    stmt, ok := d.stmts[name]
    if !ok {
        panic("statement '" + name + "' doesn't exist")
    }
    return stmt
}


// Scannable is implemented by sql.Rows & sql.Row, so we only have to write one
// FromRow() function for each datatype
type Scannable interface {
    Scan(dest ...any) error
}


// Category
type Category struct {
    id int
    name string
    info string
}

func (c *Category) FromRow(rows Scannable) error {
    return rows.Scan(
        &c.id,
        &c.name,
        &c.info,
    )
}

func CategoryByName(name string) (*Category, error) {
    row := DB.GetStatement("category_by_name").QueryRow(name)
    category := Category{}
    err := category.FromRow(row)
    if err != nil {
        return nil, err
    }

    return &category, nil
}


// Question
type Question struct {
    id int
    category int
    question string
    comment string
    answer string
}

func (q *Question) FromRow(rows *sql.Rows) error {
    return rows.Scan(
        &q.id,
        &q.category,
        &q.question,
        &q.comment,
        &q.answer,
    )
}

// Fetch at most n random questions from the given categoryID
func RandQuestions(n int, categoryID int) ([]Question, error) {

    rows, err := DB.GetStatement("n_rand_questions").Query(categoryID, n)
    if err != nil {
        return nil, err
    }
    defer rows.Close()

    questions := []Question{}
    for rows.Next() {
        q := Question{}
        err := q.FromRow(rows)
        if err != nil {
            return nil, err
        }
        questions = append(questions, q)
    }

    return questions, nil
}


// Users
type User struct {
    id int
    name string
    email string
    password string
}

func UserByNameOrMail(name, mail string) (*User, error) {
    row := DB.GetStatement("user_by_name_or_mail").QueryRow(name, mail)
    user := &User{}
    err := user.FromRow(row)
    if err != nil {
        return nil, err
    }
    return user, nil
}

func (u *User) FromRow(rows Scannable) error {
    return rows.Scan(
        &u.id,
        &u.name,
        &u.email,
        &u.password,
    )
}


// Score stores the score each user has on each question
type Score struct {
    id int
    user int
    question int
    score int
    latest time.Time
}

func (s *Score) FromRow(rows Scannable) error {
    return rows.Scan(
        &s.id,
        &s.user,
        &s.question,
        &s.score,
        &s.latest,
    )
}

func ScoreByUserAndQuestion(userID, questionID int) (*Score, error) {
    row := DB.GetStatement("score_by_user_and_question").QueryRow(userID, questionID)
    s := &Score{}
    err := s.FromRow(row)
    if err != nil {
        return nil, err
    }
    return s, nil
}

func ScoreAdd(userID, questionID, score int, latest time.Time) error {
    _, err := DB.GetStatement("score_add").Exec(
        userID,
        questionID,
        score,
        latest,
    )
    return err
}

func ScoreUpdateById(id, score int, latest time.Time) error {
    _, err := DB.GetStatement("score_update_by_id").Exec(
        score,
        latest,
    )
    return err
}
