// types.go
// HTTP interface message types
package main


// Request: Start a quiz
type MsgStartRequest struct {
    User string
    Password string
    Category string
    Count int
    Type string
}

// Response: Quiz has started
type MsgStartResponse struct {
    SessionID string
}

// Request: Give next Question
type MsgFetchRequest struct {
    SessionID string
}

// Response: Question
type MsgFetchResponse struct {
    Question string
    Comment string
    Left int
}

// Request: Answer
type MsgAnswerRequest struct {
    SessionID string
    Answer string
}

// Response: Answer
type MsgAnswerResponse struct {
    Correct bool
}

// Request: End the Quiz
type MsgEndRequest struct {
    SessionID string
}

// Response: Quiz has ended
type MsgEndResponse struct {
    Total int
    Correct int
    Wrong int
}

// Response: Error
type MsgErrorResponse struct {
    Code int
    String string
    Msg int
}


