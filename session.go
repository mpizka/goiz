// session.go implements the session datatype, functions and handlers
package main

import (
    "math/rand"
    "fmt"
    "time"
    "log"
    "database/sql"
)


// generate a pseudo-uuid
func UUID() string {
    x := "abcdefghijklmnopqrstuvwxyz" +
         "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
         "0123456789"

    s := ""

    for i := 0; i < 32; i++ {
        s += string(x[rand.Intn(len(x))])
    }

    return s 
}

// Session represents a quiz-session
// User Messages for fetch, answer, end transmit the SessionID
// In oder to refer to a session
type Session struct {
    SessionID string

    count int   // total count of questions
    current int // current question
    stype string // type of session (random, best, worst, recent, distant)
    waiting bool // true: waiting for answer (no fetch possible)
    user *User
    created time.Time
    questions []Question
    results []bool

}

// Open a new session after validating the user
func NewSession(msr *MsgStartRequest) (*Session, error) {
    // check if user exists
    user, err := UserByNameOrMail(msr.User, msr.User)
    if err != nil {
        return nil, err
    }

    // Check if user Password is correct
    // FIXME: To be implemented

    // check if category exists
    category, err := CategoryByName(msr.Category)
    if err != nil {
        return nil, err
    }

    // check count
    if msr.Count <= 0 {
        return nil, fmt.Errorf("bad count")
    }

    // get questions
    var questions []Question
    switch msr.Type {
    case "random":
        questions, err = RandQuestions(msr.Count, category.id)
        if err != nil {
            return nil, err
        }
    }

    // Create session 
    s := Session{
        SessionID: UUID(),
        count: msr.Count,
        current: 0,
        stype: msr.Type,
        user: user,
        created: time.Now(),
        questions: questions,
    }

    log.Printf("%+v", s)
    return &s, nil
}


// Update User scores
func (s *Session) updateScores() error {
    
    // FIXME: this is pretty inefficient
    // FIXME: this also isn't a transaction, which it probably should be
    // FIXME: there is also a weird bug in the score updating
    for i, r := range s.results {

        x := 1
        if !r {
            x = -1
        }

        userID := s.user.id
        questionID := s.questions[i].id

        // If no score entry exists, insert it
        score, err := ScoreByUserAndQuestion(userID, questionID)
        if err == sql.ErrNoRows {
            err := ScoreAdd(userID, questionID, x, time.Now())
            if err != nil {
                return err
            }
            continue
        }
        if err != nil {
            return err
        }
        // Otherwise, update it
        err = ScoreUpdateById(score.id, score.score + x, time.Now())
        if err != nil {
            return err
        }

    }
    return nil

}
