package main

import (
    "log"
    _"database/sql"
    "time"
    "math/rand"

    // sqlite3 driver (CGO)
    _ "github.com/mattn/go-sqlite3"
)

    
// Globally accessible database
var DB *Database

// Global session storage
var SESSIONS map[string]*Session


// Adds all prepared statements to the database
func prepStatements(d *Database) error {

    statements := map[string]string {
        "category_by_name":
            "SELECT * FROM categories WHERE name=?",
        "n_rand_questions":`
            SELECT * FROM questions WHERE question_id IN (
                SELECT question_id FROM questions
                WHERE category_id = ?
                ORDER BY RANDOM()
                LIMIT ?
            )`,
        "user_by_name_or_mail": 
            "SELECT * FROM users WHERE name=? OR email=?",
        "score_by_user_and_question":
            "SELECT * FROM score WHERE user_id = ? AND question_id = ?",
        "score_add":`
            INSERT INTO score (user_id, question_id, score, latest)
            VALUES (?, ?, ? ,?)`,
        "score_update_by_id":
            "UPDATE score SET score=?, latest=?",
    }

    for name, stmt := range statements {
        err := d.AddStatement(name, stmt)
        if err != nil {
            return err
        }
    }

    return nil

}


func main() {

    var err error

    // seed RNG
    rand.Seed(time.Now().UnixNano())

    // Create Database
    DB, err = NewDatabase("sqlite3", "goiz.db")
    if err != nil {
        log.Fatal(err)
    }
    defer DB.Close()

    // Add prepped statements
    err = prepStatements(DB)
    if err != nil {
        log.Print(err)
        return
    }

    // Init sessions map
    SESSIONS = make(map[string]*Session)

    // start webserver
    err = runServer()
    log.Print(err)
    return

}
