// server.go: implement webserver and request-handlers
package main

import (
    "net/http"
    "encoding/json"
    "time"
    "io"
)


// decodeRequest into the given datatype
func decodeRequest(r *http.Request, dt any) error {
    raw, err := io.ReadAll(r.Body)
    if err != nil {
        return err
    }
    err = json.Unmarshal(raw, dt)
    if err != nil {
        return err
    }
    return nil
}


// respondJson responds with the object marshaled to JSON
func respondJson(w http.ResponseWriter, obj any) error {
    json, err := json.Marshal(obj)
    if err != nil {
        http.Error(w, err.Error(), 500)
        return err
    }

    _, err = w.Write(json)
    if err != nil {
        http.Error(w, err.Error(), 500)
        return err
    }

    return nil
}


func HandlerStart(w http.ResponseWriter, r *http.Request) {

    // Decode the request
    msr := &MsgStartRequest{}
    err := decodeRequest(r, msr)
    if err != nil {
        http.Error(w, err.Error(), 400)
        return
    }

    // Create new session
    s, err := NewSession(msr)
    if err != nil {
        http.Error(w, err.Error(), 400)
        return
    }

    // Add new session
    // FIXME: There is currently no mechanism to delete old sessions!
    SESSIONS[s.SessionID] = s

    respondJson(w, s)

}


// Handle POST /fetch : The client requesting the next question
func HandlerFetch(w http.ResponseWriter, r *http.Request) {
    
    // Decode request
    mfr := &MsgFetchRequest{}
    err := decodeRequest(r, mfr)
    if err != nil {
        http.Error(w, err.Error(), 400)
        return
    }

    // Get Session
    s, ok := SESSIONS[mfr.SessionID]
    if !ok {
        http.Error(w, "invalid session ID", 400)
        return
    }

    // get next question, prep response obj
    if s.waiting {
        http.Error(w, "waiting for answer", 400)
        return
    }
    if s.current >= s.count {
        http.Error(w, "no more questions", 400)
        return
    }
    q := s.questions[s.current]

    response := MsgFetchResponse{
        Question: q.question,
        Comment: q.comment,
        Left: s.count - s.current+1,
    }

    // send response, incr. question marker
    err = respondJson(w, response)
    if err == nil {
        s.waiting = true
    }

}

// Handle POST /answer : Client answers the current question
func HandlerAnswer(w http.ResponseWriter, r *http.Request) {

    // Decode request
    mar := &MsgAnswerRequest{}
    err := decodeRequest(r, mar)
    if err != nil {
        http.Error(w, err.Error(), 400)
        return
    }

    // Get Session
    s, ok := SESSIONS[mar.SessionID]
    if !ok {
        http.Error(w, "invalid session ID", 400)
        return
    }

    // Check if waiting
    if !s.waiting {
        http.Error(w, "not waiting for an answer", 400)
        return
    }

    // Check if answer is correct, prep response
    correct := mar.Answer == s.questions[s.current].answer
    response := MsgAnswerResponse{Correct: correct}

    err = respondJson(w, response)
    if err == nil {
        s.current++
        s.waiting = false
        s.results = append(s.results, correct)
    }

}


// Handle POST /end : Client ends session
func HandlerEnd(w http.ResponseWriter, r *http.Request) {

    // Decode request
    mer := &MsgEndRequest{}
    err := decodeRequest(r, mer)
    if err != nil {
        http.Error(w, err.Error(), 400)
        return
    }

    // Get Session
    s, ok := SESSIONS[mer.SessionID]
    if !ok {
        http.Error(w, "invalid session ID", 400)
        return
    }

    // Update scores
    err = s.updateScores()
    if err != nil {
        http.Error(w, err.Error(), 500)
        delete(SESSIONS, mer.SessionID)
        return
    }

    correct := 0
    wrong := 0
    for _, r := range s.results {
        if r {
            correct++
        } else {
            wrong++
        }
    }

    response := MsgEndResponse{
        Total: len(s.results),
        Correct: correct,
        Wrong: wrong,
    }

    err = respondJson(w, response)

    // if all went well, delete the session
    // FIXME: We need a mechanism to delete abandoned sessions automatically
    if err == nil {
        delete(SESSIONS, mer.SessionID)
    }

}


// runServer is called by main() to start the webserver
func runServer() error {

    // register handlers to the multiplexer
    mux := http.ServeMux{}
    mux.HandleFunc("/start", HandlerStart)
    mux.HandleFunc("/fetch", HandlerFetch)
    mux.HandleFunc("/answer", HandlerAnswer)
    mux.HandleFunc("/end", HandlerEnd)


    s := &http.Server{
        Addr:           ":8080",
        Handler:        &mux,
        ReadTimeout:    10 * time.Second,
        WriteTimeout:   10 * time.Second,
        MaxHeaderBytes: 1 << 20,
    }

    return s.ListenAndServe()

}
