# goiz - A learning card server built in Go

`goiz` implements a learning-card server to be used with various frontends.


# Licence and Contribution

This project is under the MIT License (see details [here][./LICENSE]). If you
want to contribute to this project in any way, feel free to open a pull request
or reach out to me at mpizka@icloud.com.


# Learning cards

Learning-cards is a common technique among students of many topics to
successively improve memoization of knowledge like vocabulary, formulae, and so
on. The common physical implementation is small paper/cardboard cards with the
question (eg. a word in a foreign language, or the name of a chemical compound)
on one side, and the answer (eg. the translation of the word, or the structural
formula) on the other.

The student will go through the cards, with the question-side facing him/her.
For each card, he will answer the question and then flip the card to see if the
qanswer was correct. If it is, the card goes on one stack, if it isn't the card
goes on another.

When repeating the process, the ordering of the card helps the student to test
him/herself against the questions he struggled the most with previously.

As the learning progresses, new cards can be added on the "unanswered" pile.
Over time, the student can build a solid memory of the cards content.
