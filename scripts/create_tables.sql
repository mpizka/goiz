-- Activate sqlite foreign key support
PRAGMA FOREIGN_KEYS = ON;

-- Create question-categories-table
CREATE TABLE categories (
    category_id INTEGER PRIMARY KEY,
    name TEXT,
    info TEXT
);

-- Create questions-table
CREATE TABLE questions (
    question_id INTEGER PRIMARY KEY,
    category_id INTEGER,
    question1 TEXT,
    question2 TEXT,
    answer TEXT,

    FOREIGN KEY (category_id) REFERENCES categories(category_id) ON DELETE CASCADE
);

-- Create user-table
CREATE TABLE users (
    user_id INTEGER PRIMARY KEY,
    name TEXT,
    email TEXT,
    password TEXT
);

-- Create score-table
CREATE TABLE score (
    score_id INTEGER PRIMARY KEY,
    user_id INTEGER,
    question_id INTEGER,
    score INTEGER,
    latest DATETIME,
    UNIQUE (user_id, question_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,
    FOREIGN KEY (question_id) REFERENCES questions(question_id) ON DELETE CASCADE
);

